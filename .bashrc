# ROS
if [ "$(grep Ubuntu /etc/issue | awk '{print $1}')" == "Ubuntu" ]; then
	source /opt/ros/humble/setup.bash
fi

# Starship
eval "$(starship init bash)"

# Zoxide
eval "$(zoxide init bash)"

# Go
export GOPATH="$HOME/.local/share/go"
export PATH="/home/$USER/.local/share/go/bin:$PATH"
export PATH="/home/$USER/.luarocks:$PATH"

# GoLang
export GOROOT=/home/scientiac/.go
export PATH=$GOROOT/bin:$PATH
export GOPATH=/home/scientiac/.local/share/go
export PATH=$GOPATH/bin:$PATH

# Spicetify
export PATH=$PATH:/home/scientiac/.spicetify

# Emacs
export PATH="/home/$USER/.config/emacs/bin:$PATH"

# NeoVim (Meson)
export PATH="home/$USER/.local/share/nvim/mason/bin:$PATH"

# ALias
# spotify
alias spotify="distrobox enter ros -- spotify & exit"
# ls
alias "hn"="xdg-open steam://rungameid/367520"
alias "cd"="z"
alias "ls"="eza --group-directories-first --icons --color=always"
alias "imls"="wezterm imgcat --width 80"
alias "rm"="trash"
# alias "nvim"="emacsclient --tty"
# alias "nvim"="hx"
alias roboprint="lpr -P HL1110"
alias "view"="eza --icons | rofi -dmenu | sed s/^.//g | xargs sushi"
alias "tess"="$HOME/.config/tesseract/nixos/tess"

# Created by `pipx` on 2023-09-12 17:44:23
export PATH="$PATH:/home/scientiac/.local/bin"
alias config='git --git-dir=/home/scientiac/.config/tesseract/config/ --work-tree=/home/scientiac'

# HELPS
# sudo virsh net-start default
