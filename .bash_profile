# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs
# export PATH=$PATH:$HOME/.local/bin

# for rust
if [ "$(grep Ubuntu /etc/issue | awk '{print $1}')" != "Ubuntu" ]; then
  . "$HOME/.cargo/env"
fi

# # for spicetify
# export PATH=$PATH:/home/scientiac/.spicetify

# for "clear" to work inside distrobox
# export TERM=linux
#
# # For fcitx
# export GTK_IM_MODULE='fcitx'
# export QT_IM_MODULE='fcitx'
# export SDL_IM_MODULE='fcitx'
# export XMODIFIERS='@im=fcitx'

# Created by `pipx` on 2023-09-12 17:44:23
export PATH="$PATH:/home/scientiac/.local/bin"
