# nixOS Configuration

Using git bare repo for Dotfiles  
[Reference](https://www.atlassian.com/git/tutorials/dotfiles)

**configuration.nix migrated to simple flake.nix**
```
 .config/tesseract/nixos 
```

## Screenshots

![nixos_gruvbox](.config/screenshots/nixos_gruvbox.png)
![nixos_gruvbox_2](.config/screenshots/nixos_gruvbox_2.png)
![nixos_gruvbox_3](.config/screenshots/nixos_gruvbox_3.png)
