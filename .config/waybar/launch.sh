#!/usr/bin/env bash

killall -r waybar
while pgrep -x waybar >/dev/null; do sleep 1; done
waybar &
