
{ lib, fetchzip }:

fetchzip {

  name = "Material-Design-Iconic-Font";

  url = "https://gitlab.com/scientiac/tesseract.nixos/-/raw/master/.config/tesseract/nixos/fonts/Material-Design-Iconic-Font.zip";

  postFetch = ''
    downloadedFile="/build/Material-Design-Iconic-Font.zip"
    mkdir -p $out/share/fonts
    # unzip -j $downloadedFile \*.otf -d $out/share/fonts/opentype
    unzip -j $downloadedFile \*.ttf -d $out/share/fonts/truetype
  '';

  sha256 = "sha256-juX/h1nHzjbignrk0ZA6Ujsn3Xm+gmjXsozMRne5AWc=";

  meta = with lib; {
    description = "A MPV ModernX Font";
    platforms = platforms.all;
  };
}
