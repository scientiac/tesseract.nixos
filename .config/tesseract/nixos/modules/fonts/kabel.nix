{ lib, fetchzip }:

fetchzip {

  name = "kabel";

  url = "https://gitlab.com/scientiac/tesseract.nixos/-/raw/master/.config/tesseract/nixos/fonts/kabel.zip";

  postFetch = ''
    downloadedFile="/build/kabel.zip"
    mkdir -p $out/share/fonts
    # unzip -j $downloadedFile \*.otf -d $out/share/fonts/opentype
    unzip -j $downloadedFile \*.ttf -d $out/share/fonts/truetype
  '';

  sha256 = "sha256-893+VRji6aK8NxBbtsaOoqxwkG/36aqg4MI0FOfA2H0=";

  meta = with lib; {
    description = "A KDE Promo Typeface";
    platforms = platforms.all;
  };
}
