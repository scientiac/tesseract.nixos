{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [ virt-viewer ];

  programs.virt-manager.enable = true;
  virtualisation = {
    libvirtd.enable = true;
    podman = {
      enable = true;
      dockerCompat = true;
      defaultNetwork.settings.dns_enabled = true;
    };
  };
}
