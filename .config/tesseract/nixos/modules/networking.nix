{
  networking = {
    networkmanager.enable = true;
    hostName = "tesseract";

    firewall = {
      allowedTCPPorts = [ 1883 ]; # MQTT
      enable = true;
      allowedTCPPortRanges = [
        {
          from = 1714;
          to = 1764;
        }
      ];
      allowedUDPPortRanges = [
        {
          from = 1714;
          to = 1764;
        }
      ];
    };
  };
}
