{ pkgs, ... }:
{
  boot = {
    plymouth = {
      enable = true;
      themePackages = with pkgs; [
        (adi1090x-plymouth-themes.override { selected_themes = [ "red_loader" ]; })
      ];
      theme = "red_loader";
    };

    consoleLogLevel = 0;
    initrd.verbose = false;

    kernelModules = [
      "kvm-intel"
      "v4l2loopback"
    ];
    kernelParams = [
      "quiet"
      "splash"
      "rd.systemd.show_status=false"
      "rd.udev.log_level=3"
      "udev.log_priority=3"
      "boot.shell_on_fail"
    ];
    kernelPackages = pkgs.linuxPackages_latest;

    loader = {
      systemd-boot.consoleMode = "max";
      efi.canTouchEfiVariables = true;
      systemd-boot.enable = true;
      systemd-boot.editor = false;
    };
  };
}
