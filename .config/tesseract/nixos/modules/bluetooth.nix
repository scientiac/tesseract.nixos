{ pkgs, ... }:
{
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = true;
    package = pkgs.bluez;
    settings.Policy.AutoEnable = "true";
    settings.General = {
      Enable = "Source,Sink,Media,Socket";
      Name = "tesseract";
      ControllerMode = "dual";
      FastConnectable = "true";
      Experimental = "true";
      KernelExperimental = "true";
    };
  };

  systemd.services."bluetooth".serviceConfig = {
    StateDirectory = "";
    ReadWritePaths = "/keep/var/lib/bluetooth/";
    ConfigurationDirectoryMode = "0755";
  };

  fileSystems."/var/lib/bluetooth" = {
    device = "/keep/var/lib/bluetooth";
    options = [
      "bind"
      "noauto"
      "x-systemd.automount"
    ];
    noCheck = true;
  };

  systemd.tmpfiles.rules = [ "d /var/lib/bluetooth 700 root root - -" ];
}
