{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [ adwaita-icon-theme ];
  services.displayManager.defaultSession = "hyprland";

  programs.regreet = {
    enable = true;
    settings = {
      GTK = {
        application_prefer_dark_theme = true;
        cursor_theme_name = "Bibata-Original-Classic";
        font_name = "FantasqueSansMNerdFont 12";
        icon_theme_name = "Adwaita";
      };
    };
  };

}
