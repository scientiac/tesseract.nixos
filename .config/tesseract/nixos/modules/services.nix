{ pkgs, ... }:
{
  services = {
    pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
    };

    syncthing = {
      enable = true;
      user = "scientiac";
      dataDir = "/home/scientiac/Storage/Documents"; # Default folder for new synced folders
      configDir = "/home/scientiac/.config/syncthing"; # Folder for Syncthing's settings and keys
    };

    dbus.enable = true;

    flatpak.enable = true;

    # Enable trash protocol
    gvfs.enable = true;

    upower = {
      enable = true;
      percentageLow = 20;
      percentageCritical = 10;
    };

    tailscale.enable = true;

    openssh.enable = true;

    gnome.gnome-keyring.enable = true;

    # Specifies what to do when the laptop lid is closed and the system is on external power.
    logind.lidSwitchExternalPower = "ignore";

    kmscon = {
      enable = true;
      hwRender = true;
      extraConfig = ''
        font-name=FantasqueSansMNerdFont
        font-size=14
      '';
    };

    # Configure keymap in X11
    xserver = {
      enable = true;
      xkb.layout = "us";
      xkb.variant = "";
    };
  };

  programs.system-config-printer.enable = true;
  services = {
    printing = {
      enable = true;
      defaultShared = true;
      drivers = [
        pkgs.brlaser
        pkgs.brgenml1lpr
        pkgs.brgenml1cupswrapper
      ];
    };
  };

}
