{ pkgs, ... }:
let
  kabel = pkgs.callPackage ./fonts/kabel.nix { };
  material-design = pkgs.callPackage ./fonts/Material-Design-Iconic-Font.nix { };
in
{
  fonts = {
    fontDir.enable = true;
    fontconfig = {
      enable = true;
      defaultFonts = {
        monospace = [ "FantasqueSansMNerdFont" ];
      };
    };
    packages = with pkgs; [
      (nerdfonts.override {
        fonts = [
          "Meslo"
          "FantasqueSansMono"
          "Mononoki"
          "NerdFontsSymbolsOnly"
        ];
      })
      source-sans-pro
      open-sans
      corefonts
      lohit-fonts.nepali
      lohit-fonts.devanagari
      annapurna-sil
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      liberation_ttf
      kabel
      material-design
    ];
  };
}
