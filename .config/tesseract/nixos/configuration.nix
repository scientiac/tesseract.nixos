{ pkgs, ... }:
{
  imports = [
    ./modules/bluetooth.nix
    ./modules/boot.nix
    ./modules/fonts.nix
    ./modules/greetd.nix
    ./modules/hardware-configuration.nix
    ./modules/hyprland.nix
    ./modules/networking.nix
    ./modules/polkit.nix
    ./modules/services.nix
    ./modules/virtualization.nix
  ];

  nix = {
    package = pkgs.nixFlakes;
    settings.experimental-features = [
      "nix-command"
      "flakes"
    ];
    settings.trusted-users = [
      "root"
      "scientiac"
    ];
    settings.auto-optimise-store = true;
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  environment.variables = {
    TERM = "linux";
    TERMINAL = "alacritty";
  };

  # Console
  console = {
    keyMap = "us";
  };

  # Set time zone and internationalisation properties.
  time.timeZone = "Asia/Kathmandu";
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "en_US.UTF-8";
      LC_IDENTIFICATION = "en_US.UTF-8";
      LC_MEASUREMENT = "en_US.UTF-8";
      LC_MONETARY = "en_US.UTF-8";
      LC_NAME = "en_US.UTF-8";
      LC_NUMERIC = "en_US.UTF-8";
      LC_PAPER = "en_US.UTF-8";
      LC_TELEPHONE = "en_US.UTF-8";
      LC_TIME = "en_US.UTF-8";
    };
  };

  users.users.scientiac = {
    isNormalUser = true;
    description = "Spandan Guragain";
    extraGroups = [
      "networkmanager"
      "wheel"
      "docker"
      "dialout"
      "plugdev"
      "adbusers"
      "libvirtd"
    ];
  };

  programs = {
    adb.enable = true;
    dconf.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
      pinentryPackage = pkgs.pinentry-curses;
    };
  };

  hardware.graphics = {
    enable = true;
    enable32Bit = true;
  };

  environment.systemPackages = with pkgs; [
    android-tools
    appimage-run
    bibata-cursors
    btrfs-progs
    coreutils-full
    gnupg
    gparted
    killall
    libsecret
    polkit_gnome
    poweralertd
    sshfs
    usbutils
    wl-clipboard
    xdg-utils

    # to be moved
    libreoffice-fresh

    # NIX
    nix-prefetch-git
    home-manager
  ];

  system.stateVersion = "23.05"; # Did you read the comment?
}
