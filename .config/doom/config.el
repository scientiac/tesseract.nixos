;;; SCIENTIAC's CONFIG

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "scientiac"
      user-mail-address "scientiac@tilde.team")

(add-to-list 'default-frame-alist '(background-color . "#282c34"))

;; Setting a default theme.
;; (setq doom-theme 'doom-tokyo-night)
;; (setq doom-theme 'adwaita-dark)
(setq doom-theme 'doom-gruvbox)

;; Set Font Sizing and Else
(setq doom-font (font-spec :family "FantasqueSansMNerdFont" :size 15)
      doom-variable-pitch-font (font-spec :family "FantasqueSansMNerdFont" :size 15)
      doom-big-font (font-spec :family "FantasqueSansMNerdFont" :size 24))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

;; Use nerd icons
(use-package nerd-icons
  :custom
  ;; The Nerd Font you want to use in GUI
  ;; "Symbols Nerd Font Mono" is the default and is recommended
  ;; but you can use any other Nerd Font if you want
  (nerd-icons-font-family "NerdFontsSymbolsOnly")
  ;; (nerd-icons-font-family "FantasqueSansMNerdFont" )
  )

(use-package-hook! evil
  :pre-init
  ;;  added this line at the top of init.el ⬇️
  (setq evil-respect-visual-line-mode t) ;; sane j and k behavior
  t)

;; Max height of whichkey
(setq which-key-side-window-max-height 0.25)

(map! :leader
      (:prefix ("d" . "do")
       :desc "Spell Check with Langtool"
       "s" #'langtool-check))

(map! :leader
      (:prefix ("d" . "do")
       :desc "Check done with Langtool"
       "d" #'langtool-check-done))

(map! :leader
      (:prefix ("d" . "do")
       :desc "Interactive Correction"
       "i" #'langtool-correct-buffer))

(map! :leader
      (:prefix ("d" . "do")
       :desc "Check for correction"
       "c" #'flyspell-correct-word-before-point))

;; Ignore inside source blocks

(add-to-list 'ispell-skip-region-alist '("^#+BEGIN_SRC" . "^#+END_SRC"))

(use-package emojify
  :hook (after-init . global-emojify-mode))

;; ascii replace
(setq fancy-splash-image (concat doom-private-dir "splash.svg"))

(defun doom-dashboard-draw-ascii-emacs-banner-fn ()
  (let* ((banner
          '(" "
            " "
            " "
            " "
            " "
            " "
            "______ _____ ____ ___ ___"
            "`  _  V  _  V  _ \\|  V  ´"
            "| | | | | | | | | |     |"
            "| | | | | | | | | | . . |"
            "| |/ / \\ \\| | |/ /\\ |V| |"
            "|   /   \\__/ \\__/  \\| | |"
            "|  /                ' | |"
            "| /     E M A C S     \\ |"
            "´´                     ``"
            " "
            " "
            " "
            " "
            " "
            " "
            " "))
         (longest-line (apply #'max (mapcar #'length banner))))
    (put-text-property
     (point)
     (dolist (line banner (point))
       (insert (+doom-dashboard--center
                +doom-dashboard--width
                (concat
                 line (make-string (max 0 (- longest-line (length line)))
                                   32)))
               "\n"))
     'face 'doom-dashboard-banner)))

(unless (display-graphic-p) ; for some reason this messes up the graphical splash screen atm
  (setq +doom-dashboard-ascii-banner-fn #'doom-dashboard-draw-ascii-emacs-banner-fn))

;; Only for emacs not the client.
;; (set-frame-parameter nil 'undecorated t)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
;; Relative Line Numbers
(setq display-line-numbers-type 'relative)

(use-package! evil-terminal-cursor-changer

  :hook (tty-setup . evil-terminal-cursor-changer-activate))

;; This is just set here to prevent the white flash on emacs startup.
(set-background-color "#282c34")
(add-to-list 'default-frame-alist
             '(background-color . "#17191a"))

;; Put this to .Xresources for X11 systems:
;; Emacs.Background: #282c34

;; Put this to settings.ini on ~/.config/gtk-3.0/
;; [Settings]
;; gtk-application-prefer-dark-theme=true


;; Save and load .Xresources by executing `xrdb -merge ~/.Xresources`

(use-package! org-auto-tangle
  :defer t
  :hook (org-mode . org-auto-tangle-mode)
  :config
  (setq org-auto-tangle-default t))

;; Minimal UI
(package-initialize)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; Choose some fonts
;; (set-face-attribute 'default nil :family "Iosevka")
;; (set-face-attribute 'variable-pitch nil :family "Iosevka Aile")
;; (set-face-attribute 'org-modern-symbol nil :family "Iosevka")

;; Add frame borders and window dividers
(modify-all-frames-parameters
 '((right-divider-width . 10)
   (internal-border-width . 10)))
(dolist (face '(window-divider
                window-divider-first-pixel
                window-divider-last-pixel))
  (face-spec-reset-face face)
  (set-face-foreground face (face-attribute 'default :background)))
(set-face-background 'fringe (face-attribute 'default :background))

(setq
 ;; Edit settings
 org-auto-align-tags nil
 org-tags-column 0
 org-catch-invisible-edits 'show-and-error
 org-special-ctrl-a/e t
 org-insert-heading-respect-content t

 ;; Org styling, hide markup etc.
 org-hide-emphasis-markers t
 org-pretty-entities t
 org-ellipsis "…"

 ;; Agenda styling
 org-agenda-tags-column 0
 org-agenda-block-separator ?─
 org-agenda-time-grid
 '((daily today require-timed)
   (800 1000 1200 1400 1600 1800 2000)
   " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
 org-agenda-current-time-string
 "⭠ now ─────────────────────────────────────────────────")

(global-org-modern-mode)

(after! rustic
  (setq rustic-lsp-server 'rust-analyzer))
